@extends('../master')

@section('title','Book Title - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Book Title - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Book_Title/update']) !!}

    {!! Form::label('book_title','Book Title:') !!}
    {!! Form::text('book_title',$oneData['book_title'],['class'=>'form-control', 'required'=>'required']) !!}

    <br>

    {!! Form::label('author_name','Author Name:') !!}
    {!! Form::text('author_name',$oneData['author_name'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>


     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection