@extends('master')


@section('title','Book Title - Single Book')


@section('content')


    <h1> Single Book Information: </h1>
    <table class="table table-bordered">

       <tr> <td> Book Title</td> <td> {!! $oneData['book_title'] !!} </td> </tr>
        <tr> <td> Author Name</td> <td>{!! $oneData['author_name'] !!} </td> </tr>

     </table>


@endsection
