<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Atomic Project</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background:radial-gradient(#dda0dd,#660066);
            color: #ffffff;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #ffffff;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }


        footer{height: 30px;
            width: 100%;
            background:linear-gradient(#000000,#0f192a);
            color: white;
            text-align: center;
            font-weight: bold;
            padding: 5px;
            position: fixed;
            bottom: 0;
            left: 0;
            z-index: 100;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
                @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Atomic Project
        </div>

        <div class="links">
            <a href="Birth_Day/index">Birthday</a>
            <a href="Book_Title/index">booktitle</a>
            <a href="City/index">City</a>
            <a href="Email/index">Email</a>
            <a href="Gender/index">Gender</a>
            <a href="Hobbies/index">Hobbies</a>
            <a href="Profile_Picture/index">Profile Picture</a>
            <a href="Summary_Of_Organization/index">Summary of organization</a>
        </div>
    </div>
</div>
<footer>Copyright &copy; <?php echo date('Y');?>Atomic project created with laravel </footer>
</body>
</html>
