@extends('../master')

@section('title','Book Title - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> BirthDay - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Birth_Day/update']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

    {!! Form::label('birthday','Birth day:') !!}
    {!! Form::text('birthday',$oneData['birthday'],['class'=>'form-control', 'required'=>'required']) !!}

    <br>




     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection