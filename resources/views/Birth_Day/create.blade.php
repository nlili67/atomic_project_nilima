@extends('../master')

@section('title','Birth Day - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> BirthDay - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Birth_Day/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('birthday','Birthday:') !!}
            {!! Form::date('birthday','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection