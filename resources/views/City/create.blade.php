@extends('../master')

@section('title','City - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> City - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/City/store','class'=>'form-horizontal'])  !!}

            {!! Form::label('city','City:') !!}
            {!!  Form::select('city', ['dhaka' => 'dhaka', 'comilla' => 'comilla','noakhali' => 'noakhali','chittagong' => 'chittagong','borishal' => 'borishal'],'',['class'=>'form-control'])!!}

            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection