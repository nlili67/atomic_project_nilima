@extends('../master')

@section('title','city - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> City - Edit Form</h3>
            <hr>
    {!! Form::open(['url'=>'/City/update']) !!}

    {!! Form::label('city','city:') !!}
            {!!  Form::select('city', ['dhaka' => 'dhaka', 'comilla'=> 'comilla','noakhali' => 'noakhali','chittagong' => 'chittagong','borishal' => 'borishal'],'',['class'=>'form-control'])!!}
    <br>

     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection