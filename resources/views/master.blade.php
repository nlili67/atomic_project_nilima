<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        body{background: radial-gradient(#ffffff,#c0c0c0);
        background-size:cover }
        
        header{
            width: 100%;
            height: 50px;
            background-color: black;
            line-height: 46px;
            padding-left: 10px;
        }
        header div{display: block;
        float: left;
        color: white;
        font-size: 20px;
        font-weight: bold;
        font-family: "Monotype Corsiva";
        }

        header nav{text-align: right;
        display: block;
        float: right}
        header nav ul li{list-style: none;display: block;
        float: left;}
        header nav ul li a{text-decoration: none;color: white;padding-left: 15px; padding-right: 15px;display: block;height: 100%;width: 100%}
        header nav ul li a:hover{border-bottom: 4px solid white;
        color: white;text-decoration: none}
        .container{margin-bottom: 50px}

        footer{height: 40px;
            width: 100%;
            background:linear-gradient(#000000,#0f192a);
            color: white;
            text-align: center;
            font-weight: bold;
            padding: 10px;
            position: fixed;
            bottom: 0;
            left: 0;
            z-index: 100;
        }

    </style>
</head>
<body>
<header>
    <div>Atomic Project</div>
    <nav>
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/Birth_Day/index">Birth Day</a></li>
            <li><a href="/Book_Title/index">Book Title</a></li>
            <li><a href="/City/index">City</a></li>
            <li><a href="/Email/index">Email</a></li>
            <li><a href="/Gender/index">Gender</a></li>
            <li><a href="/Hobbies/index">Hobbies</a></li>
            <li><a href="/Profile_Picture/index">Profile picture</a></li>
            <li><a href="/Summary_Of_Organization/index">Summary of organization</a></li>
        </ul>
    </nav>
</header>
<div class="container">
@yield('content')
</div>
<footer>Copyright &copy; <?php echo date('Y');?>Atomic project created with laravel </footer>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
</body>
</html>