@extends('../master')

@section('title','Gender - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Gender/store'] , ['class'=>'form-horizontal']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('gender','Gender:') !!}<br>
            {!! Form::label('gender','male:') !!}
            {!! Form::radio('gender', 'male') !!}<br>
            {!! Form::label('gender','Female:') !!}
            {!! Form::radio('gender', 'female') !!}<br>
            {!! Form::label('gender','Other:') !!}
            {!! Form::radio('gender','other') !!}

            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection