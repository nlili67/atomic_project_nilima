@extends('../master')


@section('title','Gender - Active List')


@section('content')


    <div class="container">

        <div class="navbar">

            <a href="/gender"><button type="button" class="btn btn-primary">Add New</button></a>
        </div>

        {!! Form::open(['url'=>'Gender/search_result']) !!}


        {!! Form::text('keyword') !!}
        {!! Form::submit('Search',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}




        Total: {!! $allData->total() !!} Gender(s) <br>

        Showing: {!! $allData->count() !!} Gender(s) <br>

        {!! $allData->links() !!}




        <table class="table table-bordered table table-striped" >

            <th>Name</th>
            <th>Gender</th>

            <th>Action Buttons</th>

            @foreach($allData as $oneData)

                <tr>

                    <td>  {!! $oneData['name'] !!} </td>
                    <td>  {!! $oneData['gender'] !!} </td>


                    <td>
                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            @endforeach


        </table>
        {!! $allData->links() !!}
    </div>



@endsection