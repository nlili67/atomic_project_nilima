@extends('../master')

@section('title','Gender - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Gender/update']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('gender','Gender:') !!}<br>
            {!! Form::label('gender','male:') !!}
            {!! Form::radio('gender', 'male') !!}<br>
            {!! Form::label('gender','Female:') !!}
            {!! Form::radio('gender', 'female') !!}<br>
            {!! Form::label('gender','Other:') !!}
            {!! Form::radio('gender','other') !!}

            <br>
     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection