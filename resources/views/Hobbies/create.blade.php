@extends('../master')

@section('title','Hobbies - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Hobbies/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('hobbies','Hobbies:') !!}<br>

            {!! Form::label('hobbies','Sleeping:') !!}
            {!! Form::checkbox('hobbies[]', 'sleeping' ) !!}<br>
            {!! Form::label('hobbies','Php coding:') !!}
            {!! Form::checkbox('hobbies[]', 'Php coding' ) !!}<br>
            {!! Form::label('hobbies','Python coding:') !!}
            {!! Form::checkbox('hobbies[]', 'C coding' ) !!}<br>
            {!! Form::label('hobbies','Python coding:') !!}
            {!! Form::checkbox('hobbies[]', 'C coding' ) !!}<br>
            {!! Form::label('hobbies','Circuit solution:') !!}
            {!! Form::checkbox('hobbies[]', 'Circuit solution' ) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection