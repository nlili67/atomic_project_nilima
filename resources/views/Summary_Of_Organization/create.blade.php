@extends('../master')

@section('title','Summary of Organization - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Summary of Organization - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Summary_Of_Organization/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('summary','Summary of Organization:') !!}<br>
            {!! Form::textarea('summary', null, ['size' => '70x5']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection