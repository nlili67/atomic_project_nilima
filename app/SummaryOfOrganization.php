<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SummaryOfOrganization extends Model
{
    protected $fillable = [
        'name', 'summary'
    ];
}
