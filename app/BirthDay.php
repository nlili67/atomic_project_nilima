<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BirthDay extends Model
{
    protected $fillable = [
        'name', 'birthday'
    ];
}
