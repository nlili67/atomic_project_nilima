<?php

namespace App\Http\Controllers;

use App\Email;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function store(){

        $objModel = new Email();
        $objModel->name=$_POST['name'];
        $objModel->email=$_POST['email'];
        $status=$objModel->save();
        return redirect()->route('EmailCreate');
    }
    public function index(){

        $objEmailModel = new Email();

        $allData = $objEmailModel->paginate(5);


        return view("Email/index",compact('allData'));

    }



    public function view($id){


        $objEmailModel = new Email();


        $oneData = $objEmailModel->find($id);

        return view('Email/view',compact('oneData'));

    }




    public function edit($id){


        $objEmailModel = new Email();

        $oneData = $objEmailModel->find($id);

        return view('Email/edit',compact('oneData'));
    }




    public function update(){


        $objEmailModel = new Email();

        $oneData = $objEmailModel->find($_POST['id']);
        $oneData->name = $_POST["name"];
        $oneData->email = $_POST["email"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('emailindex');


    }



    public function delete($id){


        $objEmailModel = new Email();

        $status = $objEmailModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('emailindex');

    }


    public function search($keyword){



        $objEmailModel = new Email();

        $searchResult =  $objEmailModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("email","LIKE","%$keyword%")
            ->paginate(5);


        return view('Email/search_result',compact('emailsearchResult')) ;

    }

}
