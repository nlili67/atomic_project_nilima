<?php

namespace App\Http\Controllers;

use App\BookTitle;
use Illuminate\Http\Request;

class BookTitleController extends Controller
{
    public function store(){

       $objModel = new BookTitle();
       $objModel->book_title=$_POST['book_title'];
       $objModel->author_name=$_POST['author_name'];
       $status=$objModel->save();
      return redirect()->route('BookTitleCreate');
    }

    public function index(){

        $objBookTitleModel = new BookTitle();

        $allData = $objBookTitleModel->paginate(5);


        return view("Book_Title/index",compact('allData'));

    }



    public function view($id){


        $objBookTitleModel = new BookTitle();


        $oneData = $objBookTitleModel->find($id);

        return view('Book_Title/view',compact('oneData'));

    }




    public function edit($id){


        $objBookTitleModel = new BookTitle();

        $oneData = $objBookTitleModel->find($id);

        return view('Book_Title/edit',compact('oneData'));
    }




    public function update(){


        $objBookTitleModel = new BookTitle();

        $oneData = $objBookTitleModel->find($_POST['id']);
        $oneData->book_title = $_POST["book_title"];
        $oneData->author_name = $_POST["author_name"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('bookindex');


    }



    public function delete($id){


        $objBookTitleModel = new BookTitle();

        $status = $objBookTitleModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('bookindex');

    }


    public function search($keyword){



        $objBookTitleModel = new BookTitle();

        $searchResult =  $objBookTitleModel
            ->where("book_title","LIKE","%$keyword%")
            ->orwhere("author_name","LIKE","%$keyword%")
            ->paginate(5);


        return view('Book_Title/search_result',compact('searchResult')) ;

    }


}
