<?php

namespace App\Http\Controllers;

use App\Gender;
use Illuminate\Http\Request;

class GenderController extends Controller
{
    public function store(){

        $objModel = new Gender();
        $objModel->name=$_POST['name'];
        $objModel->gender=$_POST['gender'];
        $status=$objModel->save();
        return redirect()->route('GenderCreate');
    }
    public function index(){

        $objGenderModel = new Gender();

        $allData = $objGenderModel->paginate(5);


        return view("Gender/index",compact('allData'));

    }



    public function view($id){


        $objGenderModel = new Gender();


        $oneData = $objGenderModel->find($id);

        return view('Gender/view',compact('oneData'));

    }




    public function edit($id){


        $objGenderModel = new Gender();

        $oneData = $objGenderModel->find($id);

        return view('Gender/edit',compact('oneData'));
    }




    public function update(){


        $objGenderModel = new Gender();

        $oneData = $objGenderModel->find($_POST['id']);
        $oneData->name = $_POST["name"];
        $oneData->gender = $_POST["gender"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('genderindex');


    }



    public function delete($id){


        $objGenderModel = new Gender();

        $status = $objGenderModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('genderindex');

    }


    public function search($keyword){



        $objGenderModel = new Gender();

        $searchResult =  $objGenderModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("gender","LIKE","%$keyword%")
            ->paginate(5);


        return view('Gender/search_result',compact('gendersearchResult')) ;

    }

}
