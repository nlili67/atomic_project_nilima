<?php

namespace App\Http\Controllers;
use App\SummaryOfOrganization;
use Illuminate\Http\Request;

class SunmmaryOfOrganizationController extends Controller
{
    public function store(){

        $objModel = new SummaryOfOrganization();
        $objModel->name=$_POST['name'];
        $objModel->summary=$_POST['summary'];
        $status=$objModel->save();
        return redirect()->route('SummaryOfOrganizationCreate');
    }

    public function index(){

        $objSummaryOfOrganizationModel = new SummaryOfOrganization();

        $allData = $objSummaryOfOrganizationModel->paginate(5);


        return view("Summary_Of_Organization/index",compact('allData'));

    }



    public function view($id){


        $objSummaryOfOrganizationModel = new SummaryOfOrganization();


        $oneData = $objSummaryOfOrganizationModel->find($id);

        return view('Summary_Of_Organization/view',compact('oneData'));

    }




    public function edit($id){


        $objSummaryOfOrganizationModel = new SummaryOfOrganization();

        $oneData = $objSummaryOfOrganizationModel->find($id);

        return view('Summary_Of_Organization/edit',compact('oneData'));
    }




    public function update(){


        $objSummaryOfOrganizationModel = new SummaryOfOrganization();

        $oneData = $objSummaryOfOrganizationModel->find($_POST['id']);
        $oneData->name = $_POST["name"];
        $oneData->summary = $_POST["summary"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('summaryoforganizationindex');


    }



    public function delete($id){


        $objSummaryOfOrganizationModel = new SummaryOfOrganization();

        $status = $objSummaryOfOrganizationModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('summaryoforganizationindex');

    }


    public function search($keyword){



        $objSummaryOfOrganizationModel = new SummaryOfOrganization();

        $searchResult =  $objSummaryOfOrganizationModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("summary","LIKE","%$keyword%")
            ->paginate(5);


        return view('Summary_Of_Organization/search_result',compact('summaryoforganizationsearchResult')) ;

    }

}
