<?php

namespace App\Http\Controllers;

use App\ProfilePicture;
use Illuminate\Http\Request;

class ProfilePictureController extends Controller
{
    public function store(){

        $objModel = new ProfilePicture();
        $objModel->name=$_POST['name'];
        $objModel->profile_picture=$_POST['profile_picture'];
        $status=$objModel->save();
        return redirect()->route('ProfilePictureCreate');
    }
    public function index(){

        $objProfilePictureModel = new ProfilePicture();

        $allData = $objProfilePictureModel->paginate(5);


        return view("Profile_Picture/index",compact('allData'));

    }



    public function view($id){


        $objProfilePictureModel = new ProfilePicture();


        $oneData = $objProfilePictureModel->find($id);

        return view('Profile_Picture/view',compact('oneData'));

    }




    public function edit($id){


        $objProfilePictureModel = new ProfilePicture();

        $oneData = $objProfilePictureModel->find($id);

        return view('Profile_Picture/edit',compact('oneData'));
    }




    public function update(){


        $objProfilePictureModel = new ProfilePicture();

        $oneData = $objProfilePictureModel->find($_POST['id']);
        $oneData->name = $_POST["name"];
        $oneData->profile_picture = $_POST["profile_picture"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('profilepictureindex');


    }



    public function delete($id){


        $objProfilePictureModel = new ProfilePicture();

        $status = $objProfilePictureModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('profilepictureindex');

    }


    public function search($keyword){



        $objProfilePictureModel = new ProfilePicture();

        $searchResult =  $objProfilePictureModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("profile_picture","LIKE","%$keyword%")
            ->paginate(5);


        return view('Profile_Picture/search_result',compact('profilepicturesearchResult')) ;

    }

}
