<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTitles extends Migration
{
    public function up()
    {
        Schema::create('book_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('book_title');
            $table->string('author_name');
            $table->string('is_trashed')->default('No');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('book_titles');
    }
}
